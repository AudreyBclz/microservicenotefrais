package com.example.notefraismicroservice.service.impl;

import com.example.notefraismicroservice.dto.*;
import com.example.notefraismicroservice.entity.Category;
import com.example.notefraismicroservice.entity.ExpenseReport;
import com.example.notefraismicroservice.enums.Status;
import com.example.notefraismicroservice.repository.CategoryRepository;
import com.example.notefraismicroservice.repository.ExpenseReportRepository;
import com.example.notefraismicroservice.service.CategoryService;
import com.example.notefraismicroservice.service.Impl.CategoryServiceImpl;
import com.example.notefraismicroservice.service.Impl.ExpenseReportServiceImpl;
import com.example.notefraismicroservice.service.Impl.UploadServiceImpl;
import com.example.notefraismicroservice.tool.DtoUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ExpenseReportServiceImplTest {

    @InjectMocks
    private ExpenseReportServiceImpl expenseReportService;

    @Spy
    private DtoUtils dtoUtils;

    @Mock
    private ExpenseReportRepository _repository;

    private ExpenseReport expenseReport;
// ajout

    @Spy
    private DtoUtils<ExpenseReport, ExpenseReportDto> _dtoUtils;

    @Mock
    private DtoUtils<Category, CategoryDto> _dtoUtilsCat;

    @Mock
    private CategoryServiceImpl categoryService;

    @InjectMocks
    private UploadServiceImpl _uploadService;

    @Mock
    private CategoryRepository _categoryRepository;
    private Category category;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        category = new Category("coucou", new ArrayList<>(0));
        category.setId(0);

    }

    @Test
    void testCreateIsTrue() {

        expenseReport = new ExpenseReport();
        expenseReport.setTitle("titre");
        expenseReport.setDescription("description");
        expenseReport.setAmount(10.0);
        expenseReport.setCategory(category);

        RequestExpenseReportDto dto = new RequestExpenseReportDto();
        dto.setTitle("titre");
        dto.setDescription("description");
        dto.setAmount(10.0);

        dto.setCategory(category.getId());
        given(_repository.save(any())).willReturn(expenseReport);
        ResponseExpenseReportDto save = expenseReportService.create(dto);
        assertThat(save).isNotNull();


    }

    @Test
    void testCreateIsNull() {

        expenseReport = new ExpenseReport();
        expenseReport.setEmployee(1);
        RequestExpenseReportDto dto = new RequestExpenseReportDto();
        dto.setEmployee(1);

        when(_repository.save(ArgumentMatchers.any(ExpenseReport.class))).thenReturn(expenseReport);
        ResponseExpenseReportDto save = expenseReportService.create(dto);

//        assertThat(save.getCategory()).isNotSameAs(0);
       Assertions.assertNull(null);
    }
}
