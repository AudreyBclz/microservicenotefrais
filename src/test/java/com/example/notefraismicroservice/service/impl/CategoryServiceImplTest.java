package com.example.notefraismicroservice.service.impl;

import com.example.notefraismicroservice.dto.RequestCategoryDto;
import com.example.notefraismicroservice.dto.ResponseCategoryDto;
import com.example.notefraismicroservice.entity.Category;
import com.example.notefraismicroservice.repository.CategoryRepository;
import com.example.notefraismicroservice.service.Impl.CategoryServiceImpl;
import com.example.notefraismicroservice.tool.DtoUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.InstanceOfAssertFactories.LIST;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CategoryServiceImplTest {

    @InjectMocks
    private CategoryServiceImpl categoryService;

    @Mock
    private CategoryRepository _repository;

    //obligé d'instancié le dtoUtils et avec @Spy sinon dtoUtils sera null
    @Spy
    private DtoUtils dtoUtils;
    @BeforeEach
    void setUp(){
        MockitoAnnotations.initMocks(this);
    }


    @Test
    void testFindbyIdShouldReturnNullWhenOptionnalIsNull(){
        when(_repository.findById(156)).thenReturn(null);
        Assertions.assertNull(categoryService.findCategorybyId(156));
        Assertions.assertNull(null);
    }

    @Test
    void testFindByIdShouldReturnCategoryWhenOptionnalIsPresent(){
        Optional<Category>optionalCategory = Optional.of(new Category("transport",new ArrayList<>()));
        when(_repository.findById(156)).thenReturn(optionalCategory);
        Assertions.assertEquals(categoryService.findCategorybyId(156),optionalCategory.get());
    }

    @Test
    void testCreateReturnNullWhenGetNameIsNotNull(){
        Category category = new Category();
        category.setName("transport");
        RequestCategoryDto dto = new RequestCategoryDto("transport");
        when(_repository.save(ArgumentMatchers.any(Category.class))).thenReturn(category);
        ResponseCategoryDto categoryDto = categoryService.create(dto);
        assertThat(categoryDto.getName()).isSameAs(category.getName());
        Assertions.assertNull(null);

    }

    @Test
    void  testCreateReturnNewCategory(){
        Category category = new Category();
         category.setName("oui");
         //any() doit etre utilisé pour le save()
         given(_repository.save(any())).willReturn(category);
         ResponseCategoryDto save = categoryService.create(new RequestCategoryDto("oui"));
         assertThat(save).isNotNull();


    }

    @Test
    void testDeleteByIdIsSuccess(){
        Category category = new Category();
        category.setName("oui");
        given(_repository.existsById(0)).willReturn(true);
        String  delete = categoryService.delete(category.getId());
        assertThat(delete).isEqualTo("La catégorie a bien été supprimée");

    }

    @Test
    void testDeleteByIdIsFalse(){
        Category category = new Category();
        category.setName("oui");
        given(_repository.existsById(0)).willReturn(false);
        String  delete = categoryService.delete(category.getId());
        assertThat(delete).isEqualTo("Erreur dans la suppression");


    }

    @Test
    void testFindAll(){

        Category category = new Category("oui",new ArrayList<>());
        Category category1 = new Category("non",new ArrayList<>());
        given(_repository.findAll()).willReturn(List.of(category1,category));
        List<ResponseCategoryDto>categoryDtoList = categoryService.findAll();
        assertThat(categoryDtoList).isNotNull();


    }




}
