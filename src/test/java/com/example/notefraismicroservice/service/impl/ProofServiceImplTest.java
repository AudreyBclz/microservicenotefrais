package com.example.notefraismicroservice.service.impl;

import com.example.notefraismicroservice.entity.Category;
import com.example.notefraismicroservice.entity.ExpenseReport;
import com.example.notefraismicroservice.entity.Proof;
import com.example.notefraismicroservice.enums.Status;
import com.example.notefraismicroservice.repository.ProofRepository;
import com.example.notefraismicroservice.service.Impl.ProofServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

@ExtendWith(MockitoExtension.class)
public class ProofServiceImplTest {

    @InjectMocks
    private ProofServiceImpl proofService;

    @Mock
    private ProofRepository _repository;

    @BeforeEach
    void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testCreateProofReturnNullWhenProofIsNull(){
        Assertions.assertNull(proofService.create(null));
    }
    @Test
    void testCreateProofReturnTheProofWhenIsNotNull(){
        Proof proof = new Proof("http://localhost:8082/proofs/image1.jpg",
                new ExpenseReport("title","description",12.25, Status.INPROGRESS,1,2,new Category(),new ArrayList<>()));

        Mockito.when(_repository.save(proof)).thenReturn(proof);
        Assertions.assertEquals(proof,proofService.create(proof));
    }
}
