package com.example.notefraismicroservice.enums;

public enum Status {
    INPROGRESS("inprogress"),
    VALIDATED("validated"),
    REFUSED("refused"),
    PAID("paid");

    private final String status;

    Status(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
