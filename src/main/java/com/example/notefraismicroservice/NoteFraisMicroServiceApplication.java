package com.example.notefraismicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NoteFraisMicroServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(NoteFraisMicroServiceApplication.class, args);
	}

}
