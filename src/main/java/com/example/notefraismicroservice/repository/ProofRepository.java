package com.example.notefraismicroservice.repository;

import com.example.notefraismicroservice.entity.Proof;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProofRepository extends CrudRepository<Proof,Integer> {
}
