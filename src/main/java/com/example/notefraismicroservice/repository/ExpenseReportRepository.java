package com.example.notefraismicroservice.repository;

import com.example.notefraismicroservice.entity.ExpenseReport;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExpenseReportRepository extends CrudRepository<ExpenseReport, Integer> {

    List<ExpenseReport> findExpenseReportsByAdmin(int idAdmin);

    List<ExpenseReport> findExpenseReportsByEmployee(int idEmployee);

    List<ExpenseReport> findExpenseReportsByCategory_Id(int id);
}
