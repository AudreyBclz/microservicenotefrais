package com.example.notefraismicroservice.service;

import com.example.notefraismicroservice.entity.Proof;

public interface ProofService {
    Proof create(Proof p);
}
