package com.example.notefraismicroservice.service;

import com.example.notefraismicroservice.dto.RequestExpenseReportDto;
import com.example.notefraismicroservice.entity.ExpenseReport;
import com.example.notefraismicroservice.entity.Proof;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface UploadService {
    Proof store(MultipartFile file, ExpenseReport expenseReport) throws IOException;
}
