package com.example.notefraismicroservice.service;

import com.example.notefraismicroservice.dto.RequestCategoryDto;
import com.example.notefraismicroservice.dto.ResponseCategoryDto;
import com.example.notefraismicroservice.entity.Category;

import java.util.List;

public interface CategoryService {
    ResponseCategoryDto findCategorybyId(int id);
    ResponseCategoryDto create(RequestCategoryDto dto);
    String delete(int id);

    List<ResponseCategoryDto>findAll();
}
