package com.example.notefraismicroservice.service;

import com.example.notefraismicroservice.dto.RequestExpenseReportDto;
import com.example.notefraismicroservice.dto.ResponseExpenseReportDto;
import com.example.notefraismicroservice.entity.ExpenseReport;
import com.example.notefraismicroservice.enums.Status;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ExpenseReportService {

 ResponseExpenseReportDto create(RequestExpenseReportDto dto);

 ResponseExpenseReportDto findById(int id);

 ResponseExpenseReportDto updateStatus(int id,RequestExpenseReportDto dto) ;

 List<ResponseExpenseReportDto>findBy(int id,String elem);

 List<ResponseExpenseReportDto> findAll();

 ResponseExpenseReportDto uploadFile(MultipartFile file,int id);

 List<ResponseExpenseReportDto>findByCategory(int id);


}
