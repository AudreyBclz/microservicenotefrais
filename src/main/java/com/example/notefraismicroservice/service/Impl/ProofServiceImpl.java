package com.example.notefraismicroservice.service.Impl;

import com.example.notefraismicroservice.entity.Proof;
import com.example.notefraismicroservice.repository.ProofRepository;
import com.example.notefraismicroservice.service.ProofService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProofServiceImpl implements ProofService {
    @Autowired
    private ProofRepository _proofRepository;

    @Override
    public Proof create(Proof p) {
        if(p!= null){
            return _proofRepository.save(p);
        }
        return null;
    }
}
