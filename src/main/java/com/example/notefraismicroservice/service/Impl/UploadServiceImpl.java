package com.example.notefraismicroservice.service.Impl;

import com.example.notefraismicroservice.entity.ExpenseReport;
import com.example.notefraismicroservice.entity.Proof;
import com.example.notefraismicroservice.repository.ExpenseReportRepository;
import com.example.notefraismicroservice.repository.ProofRepository;
import com.example.notefraismicroservice.service.UploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

@Service
public class UploadServiceImpl implements UploadService {
    private String location ="proofs";

    @Autowired
    private ExpenseReportRepository _expenseReportRepository;

    @Autowired
    private ProofRepository _proofRepository;


    @Override
    public Proof store(MultipartFile file, ExpenseReport expenseReport) throws IOException {
        if(file!=null){
            {
                Path destinationFile = Paths.get("src","main","resources","static",location).resolve(Paths.get(file.getOriginalFilename())).toAbsolutePath();
                InputStream stream = file.getInputStream();
                Files.copy(stream,destinationFile, StandardCopyOption.REPLACE_EXISTING);
                Proof proof = new Proof("http://localhost:8082/proofs/"+file.getOriginalFilename(),expenseReport);
                _proofRepository.save(proof);
                expenseReport.getProofs().add(proof);
                _expenseReportRepository.save(expenseReport);
                return proof;
            }
        }
        return null;
    }
}
