package com.example.notefraismicroservice.service.Impl;

import com.example.notefraismicroservice.dto.CategoryDto;
import com.example.notefraismicroservice.dto.RequestCategoryDto;
import com.example.notefraismicroservice.dto.ResponseCategoryDto;
import com.example.notefraismicroservice.entity.Category;
import com.example.notefraismicroservice.repository.CategoryRepository;
import com.example.notefraismicroservice.service.CategoryService;
import com.example.notefraismicroservice.tool.DtoUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    private CategoryRepository _repository;

    @Autowired
    private DtoUtils<Category, CategoryDto> _dtoUtils;
    @Override
    public ResponseCategoryDto findCategorybyId(int id) {
        Optional<Category> optCat = _repository.findById(id);
        if(optCat!=null){
            if(optCat.isPresent()){
                return (ResponseCategoryDto) _dtoUtils.convertToDto(optCat.get(),new ResponseCategoryDto());
            }
        }
        return null;
    }

    @Override
    public ResponseCategoryDto create(RequestCategoryDto dto) {
        if(!Objects.equals(dto.getName(), "")){
            Category category = _dtoUtils.convertToEntity(new Category(),dto);
            return (ResponseCategoryDto) _dtoUtils.convertToDto(_repository.save(category),new ResponseCategoryDto());
        }
        return null;
    }

    @Override
    public String delete(int id) {
        if(_repository.existsById(id)){
            _repository.deleteById(id);
            return "La catégorie a bien été supprimée";
        }else{
            return "Erreur dans la suppression";
        }

    }

    @Override
    public List<ResponseCategoryDto> findAll() {
        List<Category> categories = (List<Category>) _repository.findAll();
        List<ResponseCategoryDto> dtos = new ArrayList<>();
        if(categories.size()>0){
            for(Category c: categories){
                ResponseCategoryDto dto = (ResponseCategoryDto) _dtoUtils.convertToDto(c, new ResponseCategoryDto());
                dtos.add(dto);
            }
        }
        return dtos;
    }
}
