package com.example.notefraismicroservice.service.Impl;

import com.example.notefraismicroservice.dto.*;
import com.example.notefraismicroservice.entity.Category;
import com.example.notefraismicroservice.entity.ExpenseReport;
import com.example.notefraismicroservice.entity.Proof;
import com.example.notefraismicroservice.repository.ExpenseReportRepository;
import com.example.notefraismicroservice.service.ExpenseReportService;
import com.example.notefraismicroservice.tool.DtoUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class ExpenseReportServiceImpl implements ExpenseReportService {

    @Autowired
    private ExpenseReportRepository _expenseReportRepository;

    @Autowired
    private DtoUtils<ExpenseReport, ExpenseReportDto> _dtoUtils;

    @Autowired
    private DtoUtils<Category, CategoryDto>_dtoUtilsCat;

    @Autowired
    private UploadServiceImpl _uploadService;

    @Autowired
    private CategoryServiceImpl _categoryService;




    public ResponseExpenseReportDto create(RequestExpenseReportDto requestExpenseReportDto) {
        if (!(requestExpenseReportDto.getAmount() == 0 && requestExpenseReportDto.getDescription() != null
                && requestExpenseReportDto.getEmployee() == 0 && requestExpenseReportDto.getTitle() != null
                && requestExpenseReportDto.getCategory()!=0)) {
            ResponseExpenseReportDto dtoTemp = new ResponseExpenseReportDto();
            dtoTemp.setAdmin(requestExpenseReportDto.getAdmin());
            dtoTemp.setDate(requestExpenseReportDto.getDate());
            dtoTemp.setAmount(requestExpenseReportDto.getAmount());
            dtoTemp.setDescription(requestExpenseReportDto.getDescription());
            dtoTemp.setTitle(requestExpenseReportDto.getTitle());
            dtoTemp.setEmployee(requestExpenseReportDto.getEmployee());
            ResponseCategoryDto category = _categoryService.findCategorybyId(requestExpenseReportDto.getCategory());
            dtoTemp.setCategory(_dtoUtilsCat.convertToEntity(new Category(),category));
            dtoTemp.setStatus(requestExpenseReportDto.getStatus());

            List<Proof> proofs = new ArrayList<>();

            ExpenseReport expenseReport = _dtoUtils.convertToEntity(new ExpenseReport(),dtoTemp);

            return (ResponseExpenseReportDto) _dtoUtils.convertToDto(_expenseReportRepository.save(expenseReport), new ResponseExpenseReportDto());
        }
       else{
           return null;
        }
    }

    @Override
    public ResponseExpenseReportDto findById(int id) {
        Optional<ExpenseReport> optDto = _expenseReportRepository.findById(id);
        if(optDto.isPresent()){
            return (ResponseExpenseReportDto) _dtoUtils.convertToDto(optDto.get(),new ResponseExpenseReportDto());
        }else{
            return null;
        }

    }

    @Override
    public ResponseExpenseReportDto updateStatus(int id, RequestExpenseReportDto dto) {
        Optional<ExpenseReport> expenseReport = _expenseReportRepository.findById(id);
        if(expenseReport.isPresent()){
            if(expenseReport.get().getAdmin()==0){
                expenseReport.get().setAdmin(dto.getAdmin());
            }
            expenseReport.get().setStatus(dto.getStatus());
            return (ResponseExpenseReportDto) _dtoUtils.convertToDto(_expenseReportRepository.save(expenseReport.get()),new ResponseExpenseReportDto());
        }
        return null;
    }

    @Override
    public List<ResponseExpenseReportDto> findBy(int id,String elem) {
        List<ExpenseReport> expenseReports = new ArrayList<>();
        if(Objects.equals(elem, "admin")){
            expenseReports = _expenseReportRepository.findExpenseReportsByAdmin(id);
        } else if (Objects.equals(elem, "employee")) {
            expenseReports = _expenseReportRepository.findExpenseReportsByEmployee(id);
        }
        List<ResponseExpenseReportDto> dtos = new ArrayList<>();
        if(expenseReports.size()>0){
            for(ExpenseReport er: expenseReports){
                ResponseExpenseReportDto dto = (ResponseExpenseReportDto) _dtoUtils.convertToDto(er,new ResponseExpenseReportDto());
                dtos.add(dto);
            }
        }
        return dtos;
    }

    @Override
    public List<ResponseExpenseReportDto> findAll() {
        List<ExpenseReport> expenseReports = (List<ExpenseReport>) _expenseReportRepository.findAll();
        List<ResponseExpenseReportDto> dtos = new ArrayList<>();
        if(expenseReports.size()>0){
            for (ExpenseReport er:expenseReports){
                ResponseExpenseReportDto dto = (ResponseExpenseReportDto) _dtoUtils.convertToDto(er,new ResponseExpenseReportDto());
                dtos.add(dto);
            }
        }
        return dtos;
    }

    @Override
    public ResponseExpenseReportDto uploadFile(MultipartFile file, int id) {
        Optional<ExpenseReport> optReport = _expenseReportRepository.findById(id);
        if(optReport.isPresent()){
            try {
                _uploadService.store(file,optReport.get());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            return (ResponseExpenseReportDto) _dtoUtils.convertToDto(optReport.get(),new ResponseExpenseReportDto());
        }
        return null;
    }

    @Override
    public List<ResponseExpenseReportDto> findByCategory(int id) {
        List<ExpenseReport> expenseReports = _expenseReportRepository.findExpenseReportsByCategory_Id(id);
        List<ResponseExpenseReportDto> dtos = new ArrayList<>();
        if(!expenseReports.isEmpty()){
            for(ExpenseReport e: expenseReports){
                ResponseExpenseReportDto dto = (ResponseExpenseReportDto) _dtoUtils.convertToDto(e,new ResponseExpenseReportDto());
                dtos.add(dto);
            }
        }
        return dtos;
    }


}
