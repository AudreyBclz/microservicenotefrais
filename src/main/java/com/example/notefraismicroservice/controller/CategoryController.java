package com.example.notefraismicroservice.controller;

import com.example.notefraismicroservice.dto.RequestCategoryDto;
import com.example.notefraismicroservice.dto.ResponseCategoryDto;
import com.example.notefraismicroservice.service.Impl.CategoryServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/category")
@CrossOrigin(value = "http://localhost:8080",methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.PATCH})
public class CategoryController {
    @Autowired
    private CategoryServiceImpl _categoryService;

    @GetMapping("/{id}")
    public ResponseEntity<ResponseCategoryDto>findById(@PathVariable int id){
        return ResponseEntity.ok(_categoryService.findCategorybyId(id));
    }
    @PostMapping("")
    public ResponseEntity<ResponseCategoryDto>create(@RequestBody RequestCategoryDto dto){
        return ResponseEntity.ok(_categoryService.create(dto));
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<String>delete(@PathVariable int id){
        return ResponseEntity.ok(_categoryService.delete(id));
    }
    @GetMapping("")
    public ResponseEntity<List<ResponseCategoryDto>>findAll(){
        return ResponseEntity.ok(_categoryService.findAll());
    }
}
