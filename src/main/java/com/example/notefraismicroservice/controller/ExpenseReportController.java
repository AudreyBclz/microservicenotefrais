package com.example.notefraismicroservice.controller;

import com.example.notefraismicroservice.dto.RequestExpenseReportDto;
import com.example.notefraismicroservice.dto.ResponseExpenseReportDto;
import com.example.notefraismicroservice.service.Impl.ExpenseReportServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.util.List;

@RestController
@RequestMapping("/api/expense-report")
@CrossOrigin(value = "http://localhost:8080",methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.PATCH},
        allowedHeaders = {"Content-Type", "X-Requested-With", "accept", "Origin", "Access-Control-Request-Method", "Access-Control-Request-Headers"},
        exposedHeaders = {"Access-Control-Allow-Origin", "Access-Control-Allow-Credentials"})
public class ExpenseReportController {

    @Autowired
    private ExpenseReportServiceImpl _expenseReportService;

    @PostMapping("")
    public ResponseEntity<ResponseExpenseReportDto> create(@RequestBody RequestExpenseReportDto requestDto){
        ResponseExpenseReportDto dto = _expenseReportService.create(requestDto);
        return ResponseEntity.ok(dto);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseExpenseReportDto>findOne(@PathVariable int id){
        return  ResponseEntity.ok(_expenseReportService.findById(id));
    }

    @PatchMapping("/{id}")
    public ResponseEntity<ResponseExpenseReportDto>updateStatus(@PathVariable int id,@RequestBody RequestExpenseReportDto dto){
        return  ResponseEntity.ok(_expenseReportService.updateStatus(id,dto));
    }

    @GetMapping("/admin/{id}")
    public ResponseEntity<List<ResponseExpenseReportDto>>findByAdmin(@PathVariable int id){
        return ResponseEntity.ok(_expenseReportService.findBy(id,"admin"));
    }

    @GetMapping("/employee/{id}")
    public ResponseEntity<List<ResponseExpenseReportDto>>findByEmployee(@PathVariable int id){
        return ResponseEntity.ok(_expenseReportService.findBy(id,"employee"));
    }
    @GetMapping("category/{id}")
    public ResponseEntity<List<ResponseExpenseReportDto>>findByCategory(@PathVariable int id){
        return ResponseEntity.ok(_expenseReportService.findByCategory(id));
    }

    @GetMapping("")
    public ResponseEntity<List<ResponseExpenseReportDto>>findAll(){
        return ResponseEntity.ok(_expenseReportService.findAll());
    }

    @PostMapping(value = "upload/{id}")
    public ResponseEntity<ResponseExpenseReportDto> uploadImage(@PathVariable int id, @RequestParam MultipartFile file){
        return ResponseEntity.ok(_expenseReportService.uploadFile(file,id));
    }
}
