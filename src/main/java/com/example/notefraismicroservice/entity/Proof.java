package com.example.notefraismicroservice.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Proof {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String path;

    @ManyToOne
    @JoinColumn(name = "expense_report_id")
    private ExpenseReport expenseReport;

    public Proof(String path, ExpenseReport expenseReport) {
        this.path = path;
        this.expenseReport = expenseReport;
    }

    @Override
    public String toString() {
        return "Proof{" +
                "id=" + id +
                ", path='" + path + '\'' +
                ", expenseReport=" + expenseReport +
                '}';
    }
}
