package com.example.notefraismicroservice.entity;

import com.example.notefraismicroservice.enums.Status;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class ExpenseReport {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String title;
    private String description;
    private Double amount;
    @JsonIgnore
    @OneToMany(mappedBy = "expenseReport",fetch = FetchType.LAZY)
    private List<Proof> proofs;
    private Date date;
    private Status status;
    private int admin;
    private int employee;
    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;


    public ExpenseReport( String title, String description, Double amount, Status status, int idAdmin, int idEmployee,Category category,List<Proof> proofs) {
        this.title = title;
        this.description = description;
        this.amount = amount;
        this.status = status;
        this.admin = idAdmin;
        this.employee = idEmployee;
        this.date=new Date();
        this.category = category;
        this.proofs = proofs;
    }

    @Override
    public String toString() {
        return "ExpenseReport{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", amount=" + amount +
                ", date=" + date +
                ", status=" + status +
                ", admin=" + admin +
                ", employee=" + employee +
                ", category=" + category +
                '}';
    }
}
