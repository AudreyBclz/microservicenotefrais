package com.example.notefraismicroservice.dto;

import com.example.notefraismicroservice.entity.Category;
import com.example.notefraismicroservice.entity.Proof;
import com.example.notefraismicroservice.enums.Status;
import lombok.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ResponseExpenseReportDto implements ExpenseReportDto {
    private int id;
    private String title;
    private String description;
    private Double amount;
    private List<Proof> proofs;
    private Date date;
    private Status status;
    private int admin;
    private int employee;

    private Category category;

    @Override
    public String toString() {
        return "ResponseExpenseReportDto{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", amount=" + amount +
                ", date=" + date +
                ", status=" + status +
                ", admin=" + admin +
                ", employee=" + employee +
                ", category=" + category +
                '}';
    }

    public ResponseExpenseReportDto(int id, String title, String description, Double amount, Date date, Status status, int admin, int employee, Category category) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.amount = amount;
        this.date = date;
        this.status = status;
        this.admin = admin;
        this.employee = employee;
        this.category = category;
        this.proofs = new ArrayList<>();
    }
}
